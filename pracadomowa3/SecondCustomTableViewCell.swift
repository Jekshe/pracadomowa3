//
//  SecondCustomTableViewCell.swift
//  pracadomowa3
//
//  Created by Stanisław Paśkowski on 18.12.2016.
//  Copyright © 2016 paskowski. All rights reserved.
//

import UIKit

class SecondCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var txtLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
