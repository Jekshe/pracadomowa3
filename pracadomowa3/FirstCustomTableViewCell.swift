//
//  CustomTableViewCell.swift
//  pracadomowa3
//
//  Created by Stanisław Paśkowski on 18.12.2016.
//  Copyright © 2016 paskowski. All rights reserved.
//

import UIKit

class FirstCustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var txtLabel: UILabel!
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.contentView.backgroundColor = UIColor.clear
        // Configure the view for the selected state
    }

}
