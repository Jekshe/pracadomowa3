//
//  ViewController.swift
//  pracadomowa3
//
//  Created by Stanisław Paśkowski on 18.12.2016.
//  Copyright © 2016 paskowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var kolory = ["Davy’s Grey", "Old Lavender", "Black Shadows"]
    var koloryBgTab = [UIColor(red:0.36, green:0.31, blue:0.38, alpha:1.0), UIColor(red:0.51, green:0.42, blue:0.50, alpha:1.0), UIColor(red:0.73, green:0.67, blue:0.74, alpha:1.0)]
    var figury = ["Trójkąty", "Kwadraty", "Okręgi"]
    var figuryBgTab = ["trojkatyBg", "kwadratyBg", "okregiBg"]
    var panstwa = ["Polska", "Francja", "Japonia"]
    var flagiTab = ["Pl", "Fr", "Jp"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }

}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 134
        case 1:
            return 88
        case 2:
            return 70
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCellId") as! CustomTableViewHeaderCell
        var title = ""
        switch section {
        case 0:
            title = "Figury"
        case 1:
            title = "Kolory"
        case 2:
            title = "Państwa"
        default:
            return UIView()
        }
        headerCell.backgroundColor = UIColor.white
        headerCell.titleLbl.text = title
        return headerCell
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return figury.count
        case 1:
            return kolory.count
        case 2:
            return panstwa.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "FirstCustomCellId", for: indexPath) as! FirstCustomTableViewCell
                cell.backgroundColor = UIColor.clear
                cell.backgroundImageView.image = UIImage(named: figuryBgTab[indexPath.row])
                cell.txtLabel.text = figury[indexPath.row]
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SecondCustomCellId", for: indexPath) as! SecondCustomTableViewCell
                cell.txtLabel.text = kolory[indexPath.row]
                cell.backgroundColor = koloryBgTab[indexPath.row]
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ThirdCustomCellId", for: indexPath) as! ThirdCustomTableViewCell
                cell.txtLabel.text = panstwa[indexPath.row]
                cell.flagImageView.image = UIImage(named: "flag\(flagiTab[indexPath.row])")
                if (indexPath.row % 2 == 0) {
                    cell.backgroundColor = UIColor(red:0.81, green:0.85, blue:0.86, alpha:1.0)
                }
                return cell
            default:
                return UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Figury"
        case 1:
            return "Kolory"
        case 2:
            return "Państwa"
        default:
            return ""
        }
    }
}

